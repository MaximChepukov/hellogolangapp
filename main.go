package main

import "github.com/gin-gonic/gin"

var helloMessage = "Hello world, Maxim Chepukov"

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.String(200, helloMessage)
	})
	err := r.Run()
	if err != nil {
		return
	} // listen and serve on 0.0.0.0:8080

}
