FROM golang:1.16.6 AS build

WORKDIR /
COPY go.mod go.sum main.go ./
RUN go mod download \
&& CGO_ENABLED=0 go build main.go

FROM scratch
WORKDIR /
COPY --from=build main ./
EXPOSE 8080
ENTRYPOINT ["./main"]