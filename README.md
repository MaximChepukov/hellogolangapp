# Exam task
This is exam task for Andersen DevOps Course

## Short description
* Golang app than should return "Hello world 1" by http request
* Application should be checked and deployed by CI\CD process


## Short info about implementation

This application deploy to Amazon ECS (Elastic Container Service) and can be checked by request

```shell
curl ec2-18-223-108-14.us-east-2.compute.amazonaws.com && echo
# It should return something like this
Hello world 1
```
